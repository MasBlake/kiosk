package backend.database
import backend.ktor.KtorTest
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

fun main(args: Array<String>) {
    val ktor = KtorTest()
    ktor.setup(true)
    Database.connect("jdbc:h2:mem:test", driver = "org.h2.Driver")

    transaction {
        addLogger(StdOutSqlLogger)
        SchemaUtils.create(Cities)

        val stPeteId = Cities.insert {
            it[name] = "St. Peter"
        } get Cities.id

        println("backend.database.Cities: ${Cities.selectAll()}")
    }
}

object Cities: IntIdTable() {
    val name = varchar("name", 50)
}